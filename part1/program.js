var fs = require("fs"),
    path_to_file = process.argv[2];

fs.readFile(path_to_file, 'utf8',
    function(err, data){
        if(err) throw err;
        fs.readFile('../stop_words.txt', 'utf8', function(err1, data1){
           if(err1) throw err1;

            //Ignore stuff before this . . .

            function extract_words(obj) {
                obj["data"] = data.toLowerCase().split(/[\W_]+/);
            }

            function load_stop_words(obj){
                obj['stop_words'] = data1.split(',');
                obj['stop_words'] = obj['stop_words'].concat('a b c d e f g h i j k l m n o p q r s t u v w x y z'.split(' '));
            }

            function increment_count(obj, w){
                if(!obj['freqs']) obj['freqs'] = {};
                if(obj['freqs'].hasOwnProperty(w)) obj['freqs'][w]++;
                else obj['freqs'][w] = 1;
            }            
            
            var data_storage_obj = {
                'data': { },
                'init': function(path_to_file) { extract_words(this, path_to_file) },// lambda : load_stop_words(stop_words_obj)
                'words': function() { return data_storage_obj['data']; }//lambda word : word in stop_words_obj['stop_words']
            };

            var stop_words_obj = {
                'stop_words' : {},
                'init': function() { return load_stop_words(this) },//lambda : load_stop_words(stop_words_obj)
                'is_stop_word': function(word) { return this['stop_words'].indexOf(word) > -1; }//lambda word : word in stop_words_obj['stop_words']
            };

            var word_freqs_obj = {
                'freqs': {},
                'increment_count': function (w) { return increment_count(this, w); }, //lambda w : increment_count(word_freqs_obj,w)
                'sorted': function () {
                    console.log('sorting, please wait... (this will take one or two minutes)');
                    var sorted = [];

                    for (var i1 = 0; i1 < Object.keys(this['freqs']).length; ++i1) {
                        var key = Object.keys(this['freqs'])[i1];
                        sorted.push({key: key, value: this['freqs'][key] });
                    }
                                        
                    sorted.sort(function(a,b){
                        return b.value - a.value;
                    });
                    
                    return sorted;
                } //lambda : sorted(word_freqs_obj[’freqs’].iteritems(), key=operator.itemgetter(1), reverse=True)
            };

            data_storage_obj['init'](process.argv[2]);
            stop_words_obj['init']();

            for(var i=0; i<data_storage_obj['words']().length; ++i) {
                var w = data_storage_obj['words']()[i];
                if (!stop_words_obj['is_stop_word'](w))
                    word_freqs_obj['increment_count'](w);
            }
            
            var sorted = word_freqs_obj['sorted']();
            for(var i=0; i<sorted.length && i < 25; ++i){
                var w = sorted[i].key,
                    c = sorted[i].value;
                console.log(w + '  -  ' + c);
            }
            
        });



    });